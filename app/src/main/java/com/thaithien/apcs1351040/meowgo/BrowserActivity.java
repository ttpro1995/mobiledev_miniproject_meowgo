package com.thaithien.apcs1351040.meowgo;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import org.droidparts.widget.ClearableEditText;


public class BrowserActivity extends Activity {


    String LOG_TAG = MainActivity.class.getSimpleName();
    WebView webview;
    ClearableEditText edittextAddress;
    InputMethodManager imm ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);

        //hide keyboard
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        webview = (WebView) findViewById(R.id.mWebview);

        edittextAddress = (ClearableEditText) findViewById(R.id.edit_address);
        edittextAddress.setSelectAllOnFocus(true);//select all when address bar is focus
        //INIT
        imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);



        //reduce lag
        if (Build.VERSION.SDK_INT >= 11) {
            webview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        //click on url in web make url write in address bar and webview open new url
        webview.setWebViewClient(new MyWebViewClient());

        webview.getSettings().setJavaScriptEnabled(true);
        edittextAddress.setText("www.google.com");//default home page

        getIntentWebsite();//fill website from intent
        GoToWeb();//Go to current page on address bar


        //called when hit enter key
        edittextAddress.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    Log.i("ActionSend", "go ActionSend");
                    //go to web page
                    GoToWeb();
                    return true;
                }
                return false;
            }
        });
    }

    private void getIntentWebsite(){
        Bundle data = getIntent().getExtras();
        String website = data.getString(getResources().getString(R.string.put_extra_websie));
        edittextAddress.setText(website);
    }

    //go to webpage which is shown on address bar
    private void GoToWeb() {
        imm.hideSoftInputFromWindow(edittextAddress.getWindowToken(), 0);//hide keyboard when start loading a web
        //add http://www. if not available
        String address = edittextAddress.getText().toString();
        Log.i(LOG_TAG, "address = " + address);
        if (!address.startsWith("www.") && !address.startsWith("http://") && !address.startsWith("https://")) {
            address = "www." + address;
        }
        if (!address.startsWith("http://") && !address.startsWith("https://")) {
            address = "http://" + address;
        }

        edittextAddress.setText(address);
        webview.loadUrl(address);
        edittextAddress.clearFocus();//allow to select all on focus again

    }

    //go back to recently webpage when press back button
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
            webview.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    //rewrite link to address bar when click url in webview
    public class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            edittextAddress.setText(url);//write new url to address bar
            GoToWeb();//go to url

            return true;
        }
    }

}




