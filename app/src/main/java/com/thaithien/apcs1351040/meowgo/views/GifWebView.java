package com.thaithien.apcs1351040.meowgo.views;

import android.content.Context;
import android.webkit.WebView;
//Use code from http://droid-blog.net/2011/10/15/tutorial-how-to-play-animated-gifs-in-android-%E2%80%93-part-2/
// https://code.google.com/p/animated-gifs-in-android/
public class GifWebView extends WebView {

    public GifWebView(Context context, String path) {
        super(context);        
        
        loadUrl(path);
    }
}
