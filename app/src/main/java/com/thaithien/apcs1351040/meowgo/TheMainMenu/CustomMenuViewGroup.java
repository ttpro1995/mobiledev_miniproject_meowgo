package com.thaithien.apcs1351040.meowgo.TheMainMenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thaithien.apcs1351040.meowgo.R;

/**
 * Created by Thien on 6/30/2015.
 */
public class CustomMenuViewGroup extends LinearLayout {

    public TextView TitleContent;
    public ImageView CoverContent;


    public CustomMenuViewGroup(Context context) {
        super(context);

        //use LayoutInflater
        LayoutInflater li = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        li.inflate(R.layout.cover_title_list_item, this, true);//bind CustomViewGroup with List_item layout

        //bind
        TitleContent = (TextView) findViewById(R.id.Title_TextView);
        CoverContent = (ImageView) findViewById(R.id.Cover_ImageView);

    }
}