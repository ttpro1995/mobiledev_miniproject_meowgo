package com.thaithien.apcs1351040.meowgo.TheMainMenu;

import android.graphics.drawable.Drawable;

/**
 * Created by Thien on 6/30/2015.
 */
public class MenuData {
    private String Title;
    private Drawable Cover;


    public MenuData(Drawable cover, String title) {
        Cover = cover;
        Title = title;
    }

    //default constructor
    public MenuData() {
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Drawable getCover() {
        return Cover;
    }

    public void setCover(Drawable cover) {
        Cover = cover;
    }
}
