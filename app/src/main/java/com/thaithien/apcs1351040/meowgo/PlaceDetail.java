package com.thaithien.apcs1351040.meowgo;

import android.net.Uri;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Thien on 8/1/2015.
 */
public class PlaceDetail {
    private String LOG_TAG = PlaceDetail.class.getSimpleName();

    private String PlaceID=null;
    private String Name=null;
    private String Phone=null;
    private String International_phone = null;
    private String Address=null;
    private String Website=null;
    private String Price_level = null;
    private String Rating = null;
    private ArrayList<Photo> Photos;
    private ArrayList<String> Types;
    private String Lat;
    private String Lng;

    private Boolean hasPhoto;

    String rawJSon;

    public PlaceDetail(String rawJSon) {
        this.rawJSon = rawJSon;
        Photos = null;
        hasPhoto = false;
        parseJSON();
    }

    public String getInfomationString(){
        String result = null;
        result =   Name +"\n"+
                "Địa chỉ " + Address+"\n";

        if (Phone!=null){
            result = result+"Số điện thoại "+Phone+"\n";
        }

        if (Website!= null) {
            result = result + "Website " + Website+"\n";
        }

        return result;
    }

    public boolean hasWebSite(){
        if (Website!=null)
            return true;
        return false;

    }

    public boolean hasPhone(){
        if (Phone !=null)
            return true;
        return false;
    }

    public boolean hasPrice_level(){
        if (Price_level!=null)
            return true;
        return false;
    }

    public boolean hasRating(){
        if (Rating !=null)
            return true;
        return false;
    }

    private void parseJSON(){
        try{
            JSONObject raw = new JSONObject(rawJSon);
            JSONObject result = raw.getJSONObject("result");

            //parse placeID
            PlaceID = result.getString("place_id");

            //parse name
            Name = result.getString("name");

            //parse phone "formatted_phone_number" : "08 3895 7535"
            if (result.has("formatted_phone_number")) {
                {
                    Phone = result.getString("formatted_phone_number");
                    International_phone = result.getString("international_phone_number");
                }
            }
            //parse address "formatted_address"
            if (result.has("formatted_address"))
                Address = result.getString("formatted_address");

            //parse address website
            if (result.has("website"))
                Website = result.getString("website");

            //parse price level
            if (result.has("price_level")){
                Price_level = result.getString("price_level");
            }

            //parse rating
            if (result.has("rating")){
                Rating = result.getString("rating");
            }

            if (result.has("photos")){
            JSONArray photos_json = result.getJSONArray("photos");

            //parse photos
            Photos = new ArrayList<>();
            for (int i = 0;i<photos_json.length();i++)
            {
                Photo tmp = new Photo();
                JSONObject aPhoto = photos_json.getJSONObject(i);
                String ref = aPhoto.getString("photo_reference");
                tmp.setPhoto_reference(ref);
                Photos.add(tmp);
            }
            hasPhoto = true;
            }

            //get Lat Lng
            JSONObject geo = result.getJSONObject("geometry");
            JSONObject location = geo.getJSONObject("location");
            Lat = location.getString("lat");
            Lng = location.getString("lng");

        }catch (Exception e){
            Log.e(LOG_TAG,"error while perse JSON");
            e.printStackTrace();
        }
    }

    public ArrayList<Photo> getPhotos() {
        return Photos;
    }

    public ArrayList<String> getTypes() {
        return Types;
    }

    public boolean hasAnyPhotos(){
        return hasPhoto;
    }

    //MUST PLACE IN ASYNCTASK OR THREAD
    public static String requestPlaceJson(String placeID){
        String result = null;
        String PLACE_DETAIL_BASE_URL = "https://maps.googleapis.com/maps/api/place/details/json?";
        String  PLACEID=  "placeid";
        String KEY = "key";

        Uri Uri_builder= Uri.parse(PLACE_DETAIL_BASE_URL).buildUpon()
                .appendQueryParameter(PLACEID, placeID)
                .appendQueryParameter(KEY, "AIzaSyCCD2JaQPvi_48ng7Fd9u9SCi4eSUuRNk0").build();

        URL url = null;
        HttpURLConnection httpURLConnection = null;
        BufferedReader reader =null;
        try {

            url = new URL(Uri_builder.toString());
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();

            Log.v("PlaceDetail URL",url.toString());//Log url

            // Read the input stream into a String
            InputStream inputStream = httpURLConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();

            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line+"\n");
            }

            //write json to variable
            result  = buffer.toString();

        }
        catch (MalformedURLException e)
        {
            e.getStackTrace();
        }
        catch (IOException e)
        {
            e.getStackTrace();
        }
        return result;
    }

    public String getName() {
        return Name;
    }

    public String getPhone() {
        return Phone;
    }

    public String getInternational_phone() {
        return International_phone;
    }

    public String getAddress() {
        return Address;
    }

    public String getWebsite() {
        return Website;
    }

    public String getLng() {
        return Lng;
    }

    public String getLat() {
        return Lat;
    }

    public String getPrice_level() {
        return Price_level;
    }

    public String getRating() {
        return Rating;
    }
}
