package com.thaithien.apcs1351040.meowgo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.TypedArray;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.thaithien.apcs1351040.meowgo.TheMainMenu.CustomMenuAdapter;
import com.thaithien.apcs1351040.meowgo.TheMainMenu.MenuData;

import java.util.ArrayList;
import java.util.List;

public class MainMenuActivity extends Activity {

    //google api
    private GoogleApiClient mGoogleApiClient;
    String LOG_TAG = MainActivity.class.getSimpleName();
    private GoogleApiClient.ConnectionCallbacks connectionCallbacks;
    private GoogleApiClient.OnConnectionFailedListener failedListener;
    //request code
    int PLACE_PICKER_REQUEST = 101;


    //view
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        //Lock portrait orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        listView = (ListView) findViewById(R.id.menu_listview_id);
        createMenu();

        //prepare google api
        createGoogleAPIcallback();
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(connectionCallbacks)
                .addOnConnectionFailedListener(failedListener)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    private void createMenu(){
        final String [] menu_title = getResources().getStringArray(R.array.main_menu);
        TypedArray icon = getResources().obtainTypedArray(R.array.menu_icon);


        ArrayList<MenuData> menuDatas = new ArrayList<>();
        for (int i =0;i<menu_title.length;i++){
            MenuData tmp = new MenuData();
            tmp.setCover(icon.getDrawable(i));
            tmp.setTitle(menu_title[i]);
            menuDatas.add(tmp);
        }

        CustomMenuAdapter adapter = new CustomMenuAdapter(MainMenuActivity.this,R.layout.cover_title_list_item,menuDatas);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                menuClick(position);
            }
        });
    }

    private void menuClick(int position){
        if (position==0){
            //TODO: show place picker
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            Context context = getApplicationContext();
            try {
                startActivityForResult(builder.build(context), PLACE_PICKER_REQUEST);
            } catch (Exception e) {
                Log.e(LOG_TAG, "meow ? something meow ?");
                e.printStackTrace();
            }
        }

        if (position==1)
        {
            showNearbyList("restaurant");
        }

        if (position==2)
        {
            showNearbyList("lodging");
        }
        if (position==3)
        {
            showNearbyList("cafe");
        }
        if (position==4)
        {
           showNearbyList("gas_station");
        }
        if (position==5)
        {
            showNearbyList("convenience_store");
        }
        if (position==6)
        {
            showNearbyList("department_store");
        }
        if (position==7)
        {
            showNearbyList("park|amusement_park");
        }
        if (position==8)
        {
            showNearbyList("movie_theater");
        }
        if (position==9)
        {
            showNearbyList("museum");
        }
    }

    private void showNearbyList(String type){
        Intent intent = new Intent(MainMenuActivity.this,PlaceListActivity.class);
        intent.putExtra(getResources().getString(R.string.put_extra_place_type),type);
        startActivity(intent);
    }

    private void createGoogleAPIcallback(){
        connectionCallbacks = new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(Bundle bundle) {
                Log.i(LOG_TAG, "onConnected");
            }

            @Override
            public void onConnectionSuspended(int i) {
                Log.i(LOG_TAG,"onConnectionSuspended");
            }
        };
        failedListener = new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(ConnectionResult connectionResult) {
                Log.i(LOG_TAG,"onConnectionFailed");
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                final Place place = PlacePicker.getPlace(data, this);
                //PlacePicker.getLatLngBounds(data);
                String attribution = PlacePicker.getAttributions(data);
                Log.i(LOG_TAG, "attribution = " + attribution);
                String toastMsg = String.format("Place: %s", place.getName());
               // Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
                Log.i(LOG_TAG, "Meow " + toastMsg);
                goToPlaceDetailActivity(place.getId());

            }
        }
    }

    private void goToPlaceDetailActivity(String placeID){
        Intent intent = new Intent(MainMenuActivity.this,PlaceDetailActivity.class);
        intent.putExtra(getResources().getString(R.string.put_extra_placeID),placeID);
        startActivity(intent);
    }
}
