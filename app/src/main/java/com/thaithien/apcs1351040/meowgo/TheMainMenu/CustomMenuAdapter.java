package com.thaithien.apcs1351040.meowgo.TheMainMenu;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Thien on 6/30/2015.
 */
public class CustomMenuAdapter extends ArrayAdapter<MenuData> {
    private Context context;
    private int resource;
    private ArrayList<MenuData> objects;

    //constructor
    public CustomMenuAdapter(Context context, int resource, ArrayList<MenuData> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View Item_view = convertView;
        if (Item_view==null)
            Item_view = new CustomMenuViewGroup(getContext());

        //book data contain cover and title
        MenuData myData = objects.get(position);

        //bind view
        TextView Title = ((CustomMenuViewGroup)Item_view).TitleContent;
        ImageView Cover = ((CustomMenuViewGroup)Item_view).CoverContent;

        //Set title and cover
        Title.setText(myData.getTitle());
        Cover.setImageDrawable(myData.getCover());

        return Item_view;//return

    }
}
