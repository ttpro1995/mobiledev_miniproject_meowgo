package com.thaithien.apcs1351040.meowgo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLngBounds;

import org.apache.http.entity.StringEntity;

import java.util.ArrayList;

public class PlaceDetailActivity extends Activity {
    String LOG_TAG = PlaceDetailActivity.class.getSimpleName();

    PlaceDetail placeDetail;


    //view
    LinearLayout linearLayout;
    TextView textView;
    ImageView photoBanner;

    //Button
    Button phone_bt;
    Button website_bt;
    Button direction_bt;
    Button showOnMap_bt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_detail);

        //Lock portrait orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        linearLayout = (LinearLayout) findViewById(R.id.placeDetailLayout_id);
        textView = (TextView) findViewById(R.id.placeInfoTextview_id);
        phone_bt = (Button) findViewById(R.id.phoneButton_id);
        website_bt = (Button) findViewById(R.id.gotoWebsiteButton_id);
        direction_bt = (Button) findViewById(R.id.directionButton_id);
        direction_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callIntentDirection();
            }
        });
        showOnMap_bt = (Button) findViewById(R.id.showOnMapButton_id);
        showOnMap_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOnMap();
            }
        });
        photoBanner = (ImageView) findViewById(R.id.photoBannerImageView_id);
        photoBanner.setVisibility(View.GONE);

        new LoadPlaceData().execute();
    }

    private void getPlaceDetailFromIntent(){
        Bundle data = getIntent().getExtras();
        String placeID = data.getString(getResources().getString(R.string.put_extra_placeID));
        String place_json = PlaceDetail.requestPlaceJson(placeID);
        placeDetail = new PlaceDetail(place_json);
    }

    private void showInfomation(){
        String result = placeDetail.getInfomationString();
        textView.setText(result);
        if (placeDetail.hasAnyPhotos())
        {
            //TODO: show PHOTO banner

            //put on photo
            ArrayList<Photo> photos = placeDetail.getPhotos();
            for (int i = 0 ;i<photos.size();i++)
            {
                new LoadPhotos().execute(photos.get(i));
            }
        }

        if (placeDetail.hasPhone())
        {
            phone_bt.setVisibility(View.VISIBLE);
            phone_bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String phone = placeDetail.getInternational_phone();
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + phone));
                    startActivity(callIntent);
                }
            });
        }

        if (placeDetail.hasWebSite()){
            website_bt.setVisibility(View.VISIBLE);
            website_bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(PlaceDetailActivity.this,BrowserActivity.class);
                    intent.putExtra(getResources().getString(R.string.put_extra_websie),placeDetail.getWebsite());
                    startActivity(intent);
                }
            });
        }
    }

    private void showInformationImproved(){
        TextView placeName = (TextView) findViewById(R.id.placeName_id);
        TextView placeAddress = (TextView) findViewById(R.id.placeAddress_id);
        TextView placePhoneLabel = (TextView) findViewById(R.id.placePhoneLabel_id);
        TextView placePhone = (TextView) findViewById(R.id.placePhone_id);
        TextView placeWebsite = (TextView) findViewById(R.id.placeWebsite_id);
        TextView placeWebsiteLabel = (TextView) findViewById(R.id.placeWebsiteLabel_id);
        TextView placePriceLevelLabel = (TextView) findViewById(R.id.placePrice_level_label_id);
        TextView placePriceLevel = (TextView) findViewById(R.id.placePrice_level_id);
        TextView placeRatingLabel = (TextView) findViewById(R.id.placeRating_label_id);
        TextView placeRating = (TextView) findViewById(R.id.placeRating_id);


        placeName.setText(placeDetail.getName());
        placeAddress.setText(placeDetail.getAddress());

        if (placeDetail.hasPhone()){
            phone_bt.setVisibility(View.VISIBLE);
            phone_bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String phone = placeDetail.getInternational_phone();
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + phone));
                    startActivity(callIntent);
                }
            });
            placePhoneLabel.setVisibility(View.VISIBLE);
            placePhone.setVisibility(View.VISIBLE);
            placePhone.setText(placeDetail.getPhone());
        }

        if (placeDetail.hasWebSite()){
            website_bt.setVisibility(View.VISIBLE);
            website_bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(PlaceDetailActivity.this, BrowserActivity.class);
                    intent.putExtra(getResources().getString(R.string.put_extra_websie), placeDetail.getWebsite());
                    startActivity(intent);
                }
            });
            placeWebsiteLabel.setVisibility(View.VISIBLE);
            placeWebsite.setVisibility(View.VISIBLE);
            placeWebsite.setText(placeDetail.getWebsite());
        }

        if (placeDetail.hasPrice_level()){
            placePriceLevelLabel.setVisibility(View.VISIBLE);
            placePriceLevel.setVisibility(View.VISIBLE);
            int price = Integer.parseInt(placeDetail.getPrice_level());
            /*
0 — Free
1 — Inexpensive
2 — Moderate
3 — Expensive
4 — Very Expensive*/
            String price_str=null;
            if (price==0){
                price_str = "Miễn phí";
            }
            if (price==1){
                price_str = "Rẻ";
            }
            if (price==2){
                price_str = "Trung bình";
            }
            if (price==3){
                price_str = "Đắt";
            }
            if (price==4){
                price_str = "Rất đắt";
            }
            placePriceLevel.setText(price_str);

        }

        if (placeDetail.hasRating()){
            placeRating.setVisibility(View.VISIBLE);
            placeRatingLabel.setVisibility(View.VISIBLE);
            String ratingvalue = placeDetail.getRating();
            Double ratingNumd = Double.parseDouble(ratingvalue);
            int ratingNum = ratingNumd.intValue();
            String star_tmp = getResources().getString(R.string.white_star);

            String strResult = "";
            for (int i = 0;i<ratingNum;i++){
                strResult = strResult+star_tmp;
            }
            strResult = strResult+" "+ratingvalue;

            placeRating.setText(strResult);
        }

        if (placeDetail.hasAnyPhotos())
        {
            //TODO: show PHOTO banner
            photoBanner.setVisibility(View.VISIBLE);
            photoBanner.setImageDrawable(getResources().getDrawable(R.drawable.photo_banner));

            //put on photo
            ArrayList<Photo> photos = placeDetail.getPhotos();
            for (int i = 0 ;i<photos.size();i++)
            {
                new LoadPhotos().execute(photos.get(i));
            }
        }

    }

    private void callIntentDirection(){
        String strLat = placeDetail.getLat();
        String strLong = placeDetail.getLng();

        String url = "http://maps.google.com/maps?daddr="+strLat+","+strLong;
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse(url));
        startActivity(intent);
    }

    private void showOnMap(){
        String url = "geo:";
        String strLat = placeDetail.getLat();
        String strLong = placeDetail.getLng();
        url = url+strLat+","+strLong;
        url = url+"?z="+"20";

        Uri gmmIntentUri = Uri.parse(url);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    private class LoadPlaceData extends AsyncTask<Void,Void,Void>{
        @Override
        protected Void doInBackground(Void... params) {
            getPlaceDetailFromIntent();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //showInfomation(); replace with improved version
            showInformationImproved();
        }
    }

    private class LoadPhotos extends AsyncTask<Photo,Void,Bitmap>{
        @Override
        protected Bitmap doInBackground(Photo... params) {
            Bitmap result = params[0].requestBitmap();
            return result;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            ImageView imageView = new ImageView(PlaceDetailActivity.this);
            imageView.setImageBitmap(bitmap);
            imageView.setPadding(0,0,0,10);
            linearLayout.addView(imageView);
        }
    }



}
