package com.thaithien.apcs1351040.meowgo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Thien on 8/1/2015.
 */
public class Photo {
    String photo_reference;

    public Photo(String photo_reference) {
        this.photo_reference = photo_reference;
    }

    public Photo() {
    }


    public String getPhoto_reference() {
        return photo_reference;
    }

    public void setPhoto_reference(String photo_reference) {
        this.photo_reference = photo_reference;
    }

    public Bitmap requestBitmap(){
        Bitmap result = null;
        String PLACE_DETAIL_BASE_URL = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400";
        String  PHOTO_REF=  "photoreference";
        String KEY = "key";

        Uri Uri_builder= Uri.parse(PLACE_DETAIL_BASE_URL).buildUpon()
                .appendQueryParameter(PHOTO_REF, photo_reference)
                .appendQueryParameter(KEY, "AIzaSyBVAA99mcAJlreQfxdFBh1Ry7rmcYg3u6M").build();
        //this api key is different from api that use to request json, because 1 image / request that make 1000 request / day ran out quickly
        // so this key from other project (which have more 1000 request/day separate from place json request)

        URL url = null;
        HttpURLConnection httpURLConnection = null;
        try {

            url = new URL(Uri_builder.toString());
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();

            Log.v("URL", url.toString());//Log url

            // Read the input stream
            InputStream inputStream = httpURLConnection.getInputStream();
            result = BitmapFactory.decodeStream(inputStream);
        }
        catch (MalformedURLException e)
        {
            e.getStackTrace();
        }
        catch (IOException e)
        {
            e.getStackTrace();
        }
        return result;
    }
}
