package com.thaithien.apcs1351040.meowgo.PlaceCustomListView;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.util.Log;

import com.hahattpro.meowdebughelper.SaveFile;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Thien on 8/3/2015.
 */
public class MiniPlaceDetail {
    String LOG_TAG = MiniPlaceDetail.class.getSimpleName();

    private JSONObject result = null;
    private String PlaceID=null;
    private String Name=null;
    private String Vicinity = null;
    private String icon_link = null;
    private Bitmap icon = null;

    public MiniPlaceDetail(JSONObject result) {
        this.result = result;
        parseJSON();
    }

    private void parseJSON(){
        try{



            //parse placeID
            PlaceID = result.getString("place_id");

            //parse name
            Name = result.getString("name");

            //parse Vicinity
            Vicinity = result.getString("vicinity");

            //parse ICon
            icon_link = result.getString("icon");



        }catch (Exception e){
            Log.e(LOG_TAG, "error while perse JSON");
            e.printStackTrace();
        }
    }

    public String getPlaceID() {
        return PlaceID;
    }

    public String getName() {
        return Name;
    }

    public String getVicinity() {
        return Vicinity;
    }

    public String getIcon_link() {
        return icon_link;
    }

    public Bitmap getIcon() {
        return icon;
    }

    public static String requestPlaceSearchJson(double latitude,double longitude, String type, String radius_meter){
        String result = null;
        String PLACE_DETAIL_BASE_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
        String  PLACEID=  "placeid";
        String KEY = "key";
        String LOCATION = "location";
        String RADIUS = "radius";
        String RANK_BY ="rankby";
        String rank_by = "distance";
        String TYPES = "types";
        String radius = radius_meter;
        String strLongitude = Location.convert(longitude, Location.FORMAT_DEGREES);
        String strLatitude = Location.convert(latitude, Location.FORMAT_DEGREES);
        Log.i("LocationMeow",latitude+" "+longitude);
        String tmp_latitude_with_longitude = strLatitude+","+strLongitude;
        Uri Uri_builder= Uri.parse(PLACE_DETAIL_BASE_URL).buildUpon()
                .appendQueryParameter(LOCATION, tmp_latitude_with_longitude)//current location
                .appendQueryParameter(RADIUS, radius)//meter
                .appendQueryParameter(KEY, "AIzaSyCCD2JaQPvi_48ng7Fd9u9SCi4eSUuRNk0")//server api key
                .appendQueryParameter(TYPES,type)//type ? (restaurant ? )
                .build();


        URL url = null;
        HttpURLConnection httpURLConnection = null;
        BufferedReader reader =null;
        try {

            url = new URL(Uri_builder.toString());
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();

            Log.v("URL", url.toString());//Log url

            // Read the input stream into a String
            InputStream inputStream = httpURLConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();

            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line+"\n");
            }

            //write json to variable
            result  = buffer.toString();

        }
        catch (MalformedURLException e)
        {
            e.getStackTrace();
        }
        catch (IOException e)
        {
            e.getStackTrace();
        }
        return result;
    }

    public void requestIcon(){
        Bitmap result = null;
        Log.v("icon", "icon link = " + icon_link );//icon url
        URL url = null;
        HttpURLConnection httpURLConnection = null;
        try {

            url = new URL(icon_link);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();

            Log.v("URL", url.toString());//Log url

            // Read the input stream
            InputStream inputStream = httpURLConnection.getInputStream();
            result = BitmapFactory.decodeStream(inputStream);
        }
        catch (MalformedURLException e)
        {
            e.getStackTrace();
        }
        catch (IOException e)
        {
            e.getStackTrace();
        }
        icon = result;

    }


}
