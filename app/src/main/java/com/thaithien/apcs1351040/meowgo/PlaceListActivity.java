package com.thaithien.apcs1351040.meowgo;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.hahattpro.meowdebughelper.Mailer;
import com.hahattpro.meowdebughelper.SaveFile;
import com.thaithien.apcs1351040.meowgo.PlaceCustomListView.CustomPlaceAdapter;
import com.thaithien.apcs1351040.meowgo.PlaceCustomListView.MiniPlaceDetail;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import im.delight.android.location.SimpleLocation;

public class PlaceListActivity extends Activity {

    private SimpleLocation location;
    private double latitude;
    private double longitude;


    private ListView listView;
    private EditText radiusEditText;
    private Button SearchNow;

    Button search;

    private String Place_Type = "restaurant"; //get from intent

    //DEBUG button
    private Button mailme_bt;

    //pref
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_list);

        //Lock portrait orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //meow still finding
        hideNotFoundMsg();

        //init pref
        preferences = PreferenceManager.getDefaultSharedPreferences(PlaceListActivity.this);
        editor = preferences.edit();

        //get place type (restaurant)
        getPlaceTypeFromIntent();

        // construct a new instance of SimpleLocation
        location = new SimpleLocation(this);
        if (!location.hasLocationEnabled()) {
            // ask the user to enable location access
            SimpleLocation.openSettings(this);
        }


        listView = (ListView) findViewById(R.id.placeListView_id);
        mailme_bt = (Button) findViewById(R.id.mailmeButton_id);
        radiusEditText = (EditText) findViewById(R.id.radius_edittext_id);
        SearchNow = (Button) findViewById(R.id.searchNowButton_id);
        SearchNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storeRadiusToPref();
                new buildNearbyPlaceList().execute();
            }
        });

        //don't show keyboard
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //TODO:DEBUG button set to gone when done
        mailme_bt.setVisibility(View.GONE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        location.beginUpdates();
        requestLocation();
        loadRadiusFromPref();
        new buildNearbyPlaceList().execute();
    }

    @Override
    protected void onPause() {
        super.onPause();
        location.endUpdates();
    }


    private void requestLocation(){
         latitude = location.getLatitude();
         longitude = location.getLongitude();
    }

    private void getPlaceTypeFromIntent(){
        Bundle data = getIntent().getExtras();
        Place_Type = data.getString(getResources().getString(R.string.put_extra_place_type));
    }

    //meow me the string (meoww)
    private void mailme(String tmp){
        SaveFile saveFile = new SaveFile("search"+System.currentTimeMillis()+".json",tmp,PlaceListActivity.this);
        Mailer mailer = new Mailer(PlaceListActivity.this);
        String email = "testing.ttpro1995@yahoo.com.vn";
        File file = saveFile.getFile();
        mailer.SendMail(email,Place_Type+" "+file.getName(),"meow",file);
    }

    //call in other thread (asyntask)
    //search nearby
    //parse json
    private ArrayList<MiniPlaceDetail> requestPlaceList(){
        String radius = getRadiusFromPref();
        final String tmp_json = MiniPlaceDetail.requestPlaceSearchJson(latitude,longitude,Place_Type,radius);
        ArrayList<MiniPlaceDetail> miniPlaceDetails = new ArrayList<>();
        mailme_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //DEBUG button
                mailme(tmp_json);
            }
        });
        try {

            final JSONObject rawJson = new JSONObject(tmp_json);
            JSONArray array = rawJson.getJSONArray("results");
            for (int i = 0;i<array.length();i++){
                JSONObject object = array.getJSONObject(i);
                MiniPlaceDetail tmp_place = new MiniPlaceDetail(object);
                miniPlaceDetails.add(tmp_place);
            }
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return miniPlaceDetails;
    }

    //main thread
    private void showList(final ArrayList<MiniPlaceDetail> miniPlaceDetails){
        if (miniPlaceDetails.size()==0){
            showNotFoundMsg();
        }
        else hideNotFoundMsg();
        CustomPlaceAdapter adapter = new CustomPlaceAdapter(PlaceListActivity.this,R.layout.place_list_item,miniPlaceDetails);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //when click on item, show detail of that item
                MiniPlaceDetail tmp_place = miniPlaceDetails.get(position);
                String place_id = tmp_place.getPlaceID();
                Intent intent = new Intent(PlaceListActivity.this, PlaceDetailActivity.class);
                intent.putExtra(getResources().getString(R.string.put_extra_placeID), place_id);
                startActivity(intent);
            }
        });

    }



    private class buildNearbyPlaceList extends AsyncTask<Void,Void,ArrayList<MiniPlaceDetail>>{
        @Override
        protected ArrayList<MiniPlaceDetail> doInBackground(Void... params) {
            return requestPlaceList();
        }

        @Override
        protected void onPostExecute(ArrayList<MiniPlaceDetail> miniPlaceDetails) {
            super.onPostExecute(miniPlaceDetails);
            showList(miniPlaceDetails);
        }
    }

    private void loadRadiusFromPref(){
        String radius = preferences.getString(getResources().getString(R.string.pref_radius), "1000");
        radiusEditText.setText(radius);

    }

    private String getRadiusFromPref(){
        String radius = preferences.getString(getResources().getString(R.string.pref_radius),"1000");
        //radiusEditText.setText(radius);
        return radius;
    }

    private void storeRadiusToPref(){
        String radius = radiusEditText.getText().toString();
        editor.remove(getResources().getString(R.string.pref_radius));
        editor.commit();
        editor.putString(getResources().getString(R.string.pref_radius),radius);
        editor.commit();
    }

    //when found something
    private void hideNotFoundMsg(){
        TextView notfoundLabel = (TextView) findViewById(R.id.notfoundTextView_id);
        ImageView notfoundImageView = (ImageView) findViewById(R.id.notfoundImageView_id);
        notfoundLabel.setVisibility(View.GONE);
        notfoundImageView.setVisibility(View.GONE);
    }

    //when place list is empty
    private void showNotFoundMsg(){
        TextView notfoundLabel = (TextView) findViewById(R.id.notfoundTextView_id);
        ImageView notfoundImageView = (ImageView) findViewById(R.id.notfoundImageView_id);
        notfoundLabel.setVisibility(View.VISIBLE);
        notfoundImageView.setVisibility(View.VISIBLE);
        notfoundImageView.setImageDrawable(getResources().getDrawable(R.drawable.sad_pusheen));

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_place_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
