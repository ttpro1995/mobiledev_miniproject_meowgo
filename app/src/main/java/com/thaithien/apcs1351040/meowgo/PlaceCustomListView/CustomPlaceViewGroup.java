package com.thaithien.apcs1351040.meowgo.PlaceCustomListView;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thaithien.apcs1351040.meowgo.R;

/**
 * Created by Thien on 8/3/2015.
 */
public class CustomPlaceViewGroup extends LinearLayout {

    public ImageView iconImage;
    public TextView placeNameText;
    public TextView placeVicinityText;

    public CustomPlaceViewGroup(Context context) {
        super(context);

        //use LayoutInflater
        LayoutInflater li = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        li.inflate(R.layout.place_list_item, this, true);//bind CustomViewGroup with List_item layout

        //bind
        iconImage = (ImageView) findViewById(R.id.placeIcon_id);
        placeNameText = (TextView) findViewById(R.id.placeNameTextView_id);
        placeVicinityText = (TextView) findViewById(R.id.placeVicinityTextView_id);

    }
}
