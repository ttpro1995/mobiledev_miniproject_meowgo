package com.thaithien.apcs1351040.meowgo.PlaceCustomListView;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.thaithien.apcs1351040.meowgo.TheMainMenu.CustomMenuViewGroup;
import com.thaithien.apcs1351040.meowgo.TheMainMenu.MenuData;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Thien on 8/3/2015.
 */
public class CustomPlaceAdapter extends ArrayAdapter<MiniPlaceDetail> {
    private Context context;
    private int resource;
    private ArrayList<MiniPlaceDetail> objects;
    private String LOG_TAG = CustomPlaceAdapter.class.getSimpleName();
    //constructor
    public CustomPlaceAdapter(Context context, int resource, ArrayList<MiniPlaceDetail> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View Item_view = convertView;
        if (Item_view==null)
            Item_view = new CustomPlaceViewGroup(getContext());

        //book data contain cover and title
        final MiniPlaceDetail myData = objects.get(position);

        //bind view
        TextView Name = ((CustomPlaceViewGroup)Item_view).placeNameText;
        TextView Vicinity = ((CustomPlaceViewGroup)Item_view).placeVicinityText;
       final ImageView imageView = ((CustomPlaceViewGroup)Item_view).iconImage;

        //Set title and cover
        Name.setText(myData.getName());
        Vicinity.setText(myData.getVicinity());


        //TODO: set icon
        //imageView.setImageBitmap(myData.getIcon());
        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                myData.requestIcon();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                imageView.setImageBitmap(myData.getIcon());
            }
        }.execute();

        return Item_view;//return
    }



}
