package com.thaithien.apcs1351040.meowgo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.thaithien.apcs1351040.meowgo.views.GifDecoderView;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends Activity {

    private String LOG_TAG = MainActivity.class.getSimpleName();

    //view
    ImageView startButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        startButton = (ImageView) findViewById(R.id.startButton_id);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasInternet()){
                Intent intent = new Intent(MainActivity.this,MainMenuActivity.class);
                startActivity(intent);
                }
                else showNoConnectionDialog();

            }
        });
        startButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    startButton.setBackgroundColor(getResources().getColor(R.color.light2_red));
                }
                if (event.getAction() == MotionEvent.ACTION_UP){
                    startButton.setBackgroundColor(getResources().getColor(R.color.light1_red));
                }

                return false;
            }
        });

        displayTitle();
        displayPusheenRiding();

        //Lock portrait orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void displayPusheenRiding(){
        try{
            InputStream is = getAssets().open("pusheen_riding.gif");
            GifDecoderView view = new GifDecoderView(MainActivity.this,is);
            view.setScaleType(ImageView.ScaleType.FIT_CENTER);
            FrameLayout frameLayout = (FrameLayout) findViewById(R.id.meowFramelayout_id);
            frameLayout.addView(view);
        }catch (IOException e){
            Log.e(LOG_TAG,"error IOException");
            e.printStackTrace();
        }
    }
    private void displayTitle(){
        try{
            InputStream is = getAssets().open("meow_title_4.gif");
            GifDecoderView view = new GifDecoderView(MainActivity.this,is);
            view.setScaleType(ImageView.ScaleType.FIT_CENTER);
            FrameLayout frameLayout = (FrameLayout) findViewById(R.id.meowTitleFramelayout_id);
            frameLayout.addView(view);
        }catch (IOException e){
            Log.e(LOG_TAG,"error IOException");
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void showNoConnectionDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Meow, cho mèo tí wifi.\n3G cũng được").setPositiveButton("Meowwwwwww",null);
        AlertDialog dialog =  builder.create();
        dialog.show();
    }
    private boolean hasInternet(){
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = null;
        activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo ==null)
            return false;
        if (activeNetworkInfo.isConnected())
            return true;
        return false;
    }
}
